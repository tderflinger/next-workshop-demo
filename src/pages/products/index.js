import Product from "@/components/Product";

const ProductsPage = ({ products }) => {
  return (
    <div>
      <h1>Product Page</h1>:
    </div>
  );
};

export const getStaticProps = async () => {
  return {
    props: {
      products: [
        { name: "Product 1", price: 0, image: "/your-image.jpg", id: 1 },
        { name: "Product 2", price: 0, image: "/your-image.jpg", id: 2 },
      ],
    },
  };
};

export default ProductsPage;
