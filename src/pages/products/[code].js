import Product from "@/components/Product";

const ProductsPage = ({ product }) => {
  return (
    <div>
      <h1>Product Page</h1>:
      <Product product={product} />
    </div>
  );
};

export const getStaticPaths = async () => {
  return {
    paths: [{ params: { code: "1" } }, { params: { code: "2" } }],
    fallback: "blocking",
  };
};

const products = [
  { name: "Product 1", code: "1", id: 1, price: 0, image: "/your-image.jpg" },
  { name: "Product 2", code: "2", id: 2, price: 0, image: "/your-image.jpg" },
];

export const getStaticProps = async ({ params }) => {
  const { code } = params;

  return {
    props: {
      product: products[code-1],
    },
  };
};

export default ProductsPage;
