import Link from "next/link";
import styles from "./Header.module.css";

export const Header = ({ title }) => {
  return (
    <header className={styles.header}>
      <Link href="/products">{title}</Link>
    </header>
  );
};
