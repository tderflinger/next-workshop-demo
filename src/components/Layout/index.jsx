import { Header } from "../Header";
import { Footer } from "../Footer";

export const Layout = ({ children }) => {
  return (
    <>
      <Header title="Your Store Title"/>
      <div style={{ minHeight: "600px" }}>{children}</div>
      <Footer />
    </>
  );
};
